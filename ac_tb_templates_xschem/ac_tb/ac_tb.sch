v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
C {devices/vsource.sym} 70 -680 0 0 {name=VGND value=0}
C {devices/capa.sym} 70 -170 0 0 {name=CLOAD
value="\{cload\}"
m=1}
C {devices/isource.sym} 70 -520 0 0 {name=IBIAS value=ibias}
C {devices/gnd.sym} 70 -650 0 0 {name=l1 lab=GND}
C {ac_tran_tb/ac_tran_tb_templates_xschem/dut_model/dut_model.sym} 280 -820 0 0 {name=XDUT
spiceprefix=X}
C {devices/lab_pin.sym} 280 -920 1 0 {name=l7 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 280 -820 3 0 {name=l2 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 340 -870 2 0 {name=l3 sig_type=std_logic lab=vout}
C {devices/lab_pin.sym} 220 -880 0 0 {name=l4 sig_type=std_logic lab=vin}
C {devices/lab_pin.sym} 220 -860 0 0 {name=l5 sig_type=std_logic lab=ibias}
C {devices/lab_pin.sym} 70 -710 1 0 {name=l6 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 70 -550 1 0 {name=l8 sig_type=std_logic lab=ibias}
C {devices/lab_pin.sym} 70 -490 3 0 {name=l9 sig_type=std_logic lab=VSS}
C {devices/vsource.sym} 180 -680 0 0 {name=VSUP value=vdd}
C {devices/lab_pin.sym} 180 -710 1 0 {name=l11 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 180 -650 3 0 {name=l10 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 70 -200 1 0 {name=l13 sig_type=std_logic lab=vout}
C {devices/lab_pin.sym} 70 -140 3 0 {name=l14 sig_type=std_logic lab=VSS}
C {devices/iopin.sym} 80 -910 2 0 {name=p3 lab=VSS}
C {devices/iopin.sym} 80 -930 2 0 {name=p4 lab=VDD}
C {devices/iopin.sym} 80 -870 2 0 {name=p5 lab=vin}
C {devices/iopin.sym} 60 -810 0 0 {name=p6 lab=vout}
C {devices/iopin.sym} 80 -850 2 0 {name=p1 lab=ibias}
C {devices/code_shown.sym} 470 -280 0 0 {name=SPICE_MAIN
only_toplevel=false 

value=".save all

.control

ac dec $&fndec $&fstart $&fstop
op

write result.raw ac.all op.all

.endc"
}
C {devices/code_shown.sym} 470 -930 0 0 {name=SPICE_PARAMS 
only_toplevel=false 
value="
* NOTE: auto-generated code block,
*       created by gen sim during 
*       concrete testbench instantiation

.lib sky130.lib.spice tt
.param mc_mm_switch=0

.param cload=1nF
.param vdd=1.8
.param vindc=0.7
.param vbias=0.8
.param ibias=0
.param vinac=1
.param fstart=1.000000e+06
.param fstop=1.000000e+11
.param fndec=2.000000e+01
.param tsim=2.000000e-09
.param tstep=1.000000e-12
.param use_cload=11

"}
