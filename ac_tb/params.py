#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import ParamsBase
from sal.simulation.simulation_output import *
from sal.testbench_params import *


@dataclass
class ac_tb_params(TestbenchParamsBase):
    """
    Parameter class for ac_tb

    Args:
    ----

    dut: DUT
        inherited from TestbenchParamsBase, encapsulates the DUT lib and cell

    sch_params: TestbenchSchematicParams
        inherited from TestbenchParamsBase, encapsulates schematic related parameters

    ibias: float

    vdd: float

    fstart: float

    fstop: float

    fndec: int

    cload: float

    use_cload: bool
    """

    ibias: float
    vdd: float
    fstart: float
    fstop: float
    fndec: int
    cload: float
    use_cload: bool

    @classmethod
    def builtin_outputs(cls) -> Dict[str, SimulationOutputBase]:
        """
        Builtin outputs are merged into the effective simulation params
        """
        return {
            'vout': SimulationOutputVoltage(analysis='ac', signal='vout', quantity=VoltageQuantity.V_MAGNITUDE),
        }

    @classmethod
    def defaults(cls) -> ac_tb_params:
        return ac_tb_params(
            dut=DUT.placeholder(),  # NOTE: Framework will inject DUT
            dut_wrapper_params=None,  # NOTE: generator can decide to use a wrapper
            sch_params=TestbenchSchematicParams(
                dut_conns=[
                    DUTTerminal(term_name='vbias', net_name='vbias')
                ],
                v_sources=[
                    DCSignalSource(source_name='vbias',
                                   plus_net_name='vbias',
                                   minus_net_name='VSS',
                                   bias_value='vbias',
                                   cdf_parameters={})
                ],
                i_sources=[],
                instance_cdf_parameters={}
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={}
            ),
            ibias=0.185,
            vdd=1.0,
            fstart=1e6,
            fstop=100e9,
            fndec=20,
            cload=40e-15,
            use_cload=True
        )
